// Taller 3 - Informática - MCIC - UDFJC
//--------------------------Code based on the one developed by Diego Diego Rasero de la Fuente---diegolo.r8-(at)-gmail.com
// Definition of Variables ----------------------------------------------------------
var Vo; // Initial velocity 
var gra; // Shooting angle
var t; // Time
var g; // Gravity
var h; // variable para la altura maxima
var ai; // Variable for the angle that describes the trajectory at any instant
// Vector components of velocity at each instant
var vx; // Vector x
var vy; // Vector y
// go variable with value: true = Start button on, false = Star button off.
var go;
//create variables for the time units for the stopwatch
var s;  // segundos
//
// Variables for coordinate axes
 var x=0;
 var y=0;
 var yp=0; // y that will represent graphically the height of the canvas will be subtracted so that the origin is down.
//
 var color; //color shoot
//--------------------------------------------------------------------------------------
//We start the time as soon as the page is loaded
window.onload = setInterval(crono, 100); //

function redondeo(numero) {
    var original=parseFloat(numero);
    var result=Math.round(original*100)/100 ;
    return result;
}

function gradosaleat() {
    document.forma.grados.value=aleatorio(0,90);
    }
    
function aleatorio(inferior,superior){
    var numPos = superior - inferior;
    var aleat = Math.random() * numPos;
    aleat = Math.floor(aleat);
    return parseInt(inferior) + aleat;
    }

function colorAleatorio(){
    return "rgb(" + aleatorio(0,255) + "," + aleatorio(0,255) + "," + aleatorio(0,100) + ")";
    }

function dibuja() {
  var canvas=document.getElementById('screen');
var ctx=canvas.getContext('2d');
//------------------------------------------------------------------------------------
ctx.fillStyle= color;

    ctx.beginPath();
    ctx.arc(x/10,yp/10,.8,0,Math.PI*2,false); // circle each point
    ctx.fill();
}
//--------------------------------------------------------------------------------------------------------
function calculacoordenadas(t) {

// change degrees to radians
gra=((parseFloat(document.forma.grados.value)*Math.PI)/180); 
g=9.81;
Vo=parseFloat(document.forma.vo.value);
        //
  // Coordinate calculation   
  x=((Vo*t*(Math.cos(gra))));
  y=((Vo*t*(Math.sin(gra)))-(.5)*g*(t*t));
  yp=1490-y;
  // Vector components of velocity at each instant
  vx=(Math.cos(gra)*Vo);
  vy=(((Math.sin(gra))*(Vo))-(g*t));
  // Angle at every moment
  ai=Math.atan(vy/vx)*(180/Math.PI);
// condition to stop the shot when it reaches zero
       if (t>0) {
            if (y<=0){
                go=false;// stop
               document.getElementById("bstart").disabled=false;
            }
          }
dibuja(); 
}
// turn on the stopwatch
function start(){
    document.forma.s.value="00";
    var canvas=document.getElementById('screen');
    var ctx=canvas.getContext('2d');
    ctx.clearRect ( 0 , 0 , 500 , 500 );
    go=true;// true value for go variable
    document.getElementById("bstart").disabled=true; // Lock the start button while the function is running
    color=colorAleatorio(); // change the trace color
}
function crono(){
if (go == true) {
    //read variable seconds
    s=parseFloat(document.forma.s.value)+1;  
    // as long as they only have one digit, add a leading zero
    if(s<10) {s='0'+s;}
    // print time in seconds                       
    document.forma.s.value=s;
    calculacoordenadas(s);
    }
}